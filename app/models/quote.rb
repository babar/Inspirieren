class Quote < ApplicationRecord
  validates_uniqueness_of :quote, scope: :author

  scope :random, -> { order('RANDOM()').first }
end
