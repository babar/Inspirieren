class QuotesController < ApplicationController
  def index
    @quote = Quote.random
  end

  def refresh
    render json: Quote.random
  end
end
