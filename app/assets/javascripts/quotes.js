$(function(){
    var interval = 1000 * 60 * 10,
        body = $('body'),
        lead = $('.lead'),
        author = $('.small span'),
        width = $(window).width(),
        height = $(window).height();

    var refreshQuote = function() {
        $.getJSON('/refresh', {}, function(response) {
            lead.text(response.quote);
            author.text(response.author);
        })
    };

    var refreshBg = function() {
        var now = + new Date();
        // var bgPath = 'https://unsplash.it/'+ width +'/'+ height +'?random&time='+ now;
        var bgPath = 'https://source.unsplash.com/category/nature/'+ width +'x'+ height +'?time='+ now;
        body.css('background-image', 'url('+ bgPath +')');
    };

    var refreshPage = function() {
        refreshQuote();
        refreshBg();
        setTimeout(refreshPage, interval)
    };

    refreshPage();
});
