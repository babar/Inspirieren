Rails.application.routes.draw do
  root 'quotes#index'
  get :refresh, to: 'quotes#refresh'
end
