class CreateQuotes < ActiveRecord::Migration[5.0]
  def change
    create_table :quotes do |t|
      t.text :quote
      t.string :author
      t.string :genre
      t.timestamps

      t.index [:quote, :author], unique: true
    end
  end
end
