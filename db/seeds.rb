require 'csv'

puts '======== Save some Inspirations ========'
csv_options = {headers: true, col_sep: ';', header_converters: lambda { |h| h.downcase }}
CSV.foreach(Rails.root.join('db', 'Quotes.csv'), csv_options) do |row|
  Quote.find_or_create_by(row.to_hash)
end

puts "======== Saved #{Quote.count} Inspirations ========"
