# Inspirieren (Demo App for SuitePad)
Inspire yourself with much wow quotes...

*DEMO:* https://inspiration.ibabar.com

### Requirements
* Ruby 2.3.3
* Bundler (`gem install bundler`)
* PostgreSQL
* NodeJS (to assets)
* NginX (for Production)

### How to deploy locally
* Clone the app
* Run `bundle install`
* Create DB and migrate: `rails db:create db:schema:load`
* Load up data: `rails db:seed`
* Boot up server `rails s`
* Hit`http://localhost:3000` and be inspired.

### Deploy in Production
* Adjust values in `config/deploy/production.rb`
* Run `cap production deploy:check`, it'll report if things are not in order. Fix them.
* Deploy with Capistrano: `cap production deploy`
* Log into server and link/copy `current/config/nginx.conf` into your NginX config.
* Load up data: `rails db:seed`
* Restart Nginx and Visit your URL
* You should see the homepage.
